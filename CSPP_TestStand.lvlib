﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2018_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2018\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2018_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2018\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains a collection of CS++VIs serving as interface for NI TestStand.

Dependencies: 
- CSPP_InstrSim https://git.gsi.de/EE-LV/CSPP/CSPP_DN/CS_Workshop.git
- InstrSim https://git.gsi.de/EE-LV/Drivers/InstrSim.git

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*,!!!*Q(C=\&gt;5R&lt;B."&amp;-&lt;RDQA*+-U*ILU"?B5FEFN+8_(2"BKX%:7,&amp;#D&gt;OY+PY#O]+VDC",\#]*`*]\)U-55C+,,LNVF`MT0T]_ZI)YXNL@2;\&gt;,W_&gt;(NL^LLU":JGQ^5`TQUNN`6ZP[,-Q\N(-V$HS^J@`3PI#W[,FDHY%F_X[0N&amp;T;^&lt;#`&lt;P^VO,S\3:V_TL^2@2&amp;2314FF[F/V*%`S*%`S*%`S)!`S)!`S)!^S*X&gt;S*X&gt;S*X&gt;S)T&gt;S)T&gt;S)T@SM:/,8/1CBZ2-HES5$*I-E&amp;Q-2=EB]33?R*.Y_+L%EXA34_**0&amp;SCR*.Y%E`C34RU5_**0)EH]31?BBK3($MZHM4$]!I]A3@Q"*\!QZ1+0!%AG#Q9/"A%BI,'Y#4Q"*\!Q[E#4_!*0)%H].#MQ".Y!E`A#4RU'8=FBK&lt;PZ(A92I\(]4A?R_.Y'&amp;K/R`%Y(M@D?*B/DM@R/!BH1G&gt;Q#()[/2=Y8RS0Y_'0()`D=4S/R`(1.*[1DTP4.8UHRW.Y$)`B-4S'BS&amp;E?!S0Y4%]BI&gt;B:8A-D_%R0);(K72Y$)`B-3$'J%QP9T#DIX'2%2A?0G/VW(B+-31W&gt;KE7LWJ2KB;&lt;;B'J&amp;I@KI;M?JOIBK7[_[K;K&lt;J&lt;K*KB_H!KNQKAG588O&amp;_L%]5A&gt;K$WVI\&lt;5BFJ4%\8K8:`YQN0JJ/0RK-0BI0V_L^VOJ_VWK]VGI`6[L7G;N&amp;KNZN@!.@P]1HBY,^VSXOPOZMX(__`4JR^@PHWY@T_^O`NZ^@8=VOO:`J@_"_^'87EZ,P@I&amp;Q*'J-9!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI_IconEditor" Type="Str">49 56 48 48 56 48 49 50 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 40 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 128 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 6 67 83 43 43 84 83 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 0

</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Messages" Type="Folder">
		<Item Name="Action" Type="Folder">
			<Item Name="DCVoltage-CB Msg.lvclass" Type="LVClass" URL="../CSPP_TestStand Messages/DCVoltage-CB Msg/DCVoltage-CB Msg.lvclass"/>
			<Item Name="Dispatch DCVoltage-CB Msg.lvclass" Type="LVClass" URL="../CSPP_TestStand Messages/Dispatch DCVoltage-CB Msg/Dispatch DCVoltage-CB Msg.lvclass"/>
		</Item>
	</Item>
	<Item Name="Shared Variable" Type="Folder">
		<Item Name="SV Read" Type="Folder">
			<Item Name="SV Read Boolean Array.vi" Type="VI" URL="../SharedVariable/SV Read Boolean Array.vi"/>
			<Item Name="SV Read Boolean.vi" Type="VI" URL="../SharedVariable/SV Read Boolean.vi"/>
			<Item Name="SV Read Cdb Array.vi" Type="VI" URL="../SharedVariable/SV Read Cdb Array.vi"/>
			<Item Name="SV Read Cdb.vi" Type="VI" URL="../SharedVariable/SV Read Cdb.vi"/>
			<Item Name="SV Read Csg Array.vi" Type="VI" URL="../SharedVariable/SV Read Csg Array.vi"/>
			<Item Name="SV Read Csg.vi" Type="VI" URL="../SharedVariable/SV Read Csg.vi"/>
			<Item Name="SV Read Cxt Array.vi" Type="VI" URL="../SharedVariable/SV Read Cxt Array.vi"/>
			<Item Name="SV Read Cxt.vi" Type="VI" URL="../SharedVariable/SV Read Cxt.vi"/>
			<Item Name="SV Read Dbl Array.vi" Type="VI" URL="../SharedVariable/SV Read Dbl Array.vi"/>
			<Item Name="SV Read Dbl.vi" Type="VI" URL="../SharedVariable/SV Read Dbl.vi"/>
			<Item Name="SV Read Ext Array.vi" Type="VI" URL="../SharedVariable/SV Read Ext Array.vi"/>
			<Item Name="SV Read Ext.vi" Type="VI" URL="../SharedVariable/SV Read Ext.vi"/>
			<Item Name="SV Read I8 Array.vi" Type="VI" URL="../SharedVariable/SV Read I8 Array.vi"/>
			<Item Name="SV Read I8.vi" Type="VI" URL="../SharedVariable/SV Read I8.vi"/>
			<Item Name="SV Read I16 Array.vi" Type="VI" URL="../SharedVariable/SV Read I16 Array.vi"/>
			<Item Name="SV Read I16.vi" Type="VI" URL="../SharedVariable/SV Read I16.vi"/>
			<Item Name="SV Read I32 Array.vi" Type="VI" URL="../SharedVariable/SV Read I32 Array.vi"/>
			<Item Name="SV Read I32.vi" Type="VI" URL="../SharedVariable/SV Read I32.vi"/>
			<Item Name="SV Read I64 Array.vi" Type="VI" URL="../SharedVariable/SV Read I64 Array.vi"/>
			<Item Name="SV Read I64.vi" Type="VI" URL="../SharedVariable/SV Read I64.vi"/>
			<Item Name="SV Read Sgl Array.vi" Type="VI" URL="../SharedVariable/SV Read Sgl Array.vi"/>
			<Item Name="SV Read Sgl.vi" Type="VI" URL="../SharedVariable/SV Read Sgl.vi"/>
			<Item Name="SV Read Str Array.vi" Type="VI" URL="../SharedVariable/SV Read Str Array.vi"/>
			<Item Name="SV Read Str.vi" Type="VI" URL="../SharedVariable/SV Read Str.vi"/>
			<Item Name="SV Read U8 Array.vi" Type="VI" URL="../SharedVariable/SV Read U8 Array.vi"/>
			<Item Name="SV Read U8.vi" Type="VI" URL="../SharedVariable/SV Read U8.vi"/>
			<Item Name="SV Read U16 Array.vi" Type="VI" URL="../SharedVariable/SV Read U16 Array.vi"/>
			<Item Name="SV Read U16.vi" Type="VI" URL="../SharedVariable/SV Read U16.vi"/>
			<Item Name="SV Read U32 Array.vi" Type="VI" URL="../SharedVariable/SV Read U32 Array.vi"/>
			<Item Name="SV Read U32.vi" Type="VI" URL="../SharedVariable/SV Read U32.vi"/>
			<Item Name="SV Read U64 Array.vi" Type="VI" URL="../SharedVariable/SV Read U64 Array.vi"/>
			<Item Name="SV Read U64.vi" Type="VI" URL="../SharedVariable/SV Read U64.vi"/>
			<Item Name="SV Read Variant Array.vi" Type="VI" URL="../SharedVariable/SV Read Variant Array.vi"/>
			<Item Name="SV Read Variant.vi" Type="VI" URL="../SharedVariable/SV Read Variant.vi"/>
			<Item Name="SV Read Wfm Array.vi" Type="VI" URL="../SharedVariable/SV Read Wfm Array.vi"/>
			<Item Name="SV Read Wfm.vi" Type="VI" URL="../SharedVariable/SV Read Wfm.vi"/>
		</Item>
		<Item Name="SV Write" Type="Folder">
			<Item Name="SV Write Boolean Array.vi" Type="VI" URL="../SharedVariable/SV Write Boolean Array.vi"/>
			<Item Name="SV Write Boolean.vi" Type="VI" URL="../SharedVariable/SV Write Boolean.vi"/>
			<Item Name="SV Write Cdb Array.vi" Type="VI" URL="../SharedVariable/SV Write Cdb Array.vi"/>
			<Item Name="SV Write Cdb.vi" Type="VI" URL="../SharedVariable/SV Write Cdb.vi"/>
			<Item Name="SV Write Csg Array.vi" Type="VI" URL="../SharedVariable/SV Write Csg Array.vi"/>
			<Item Name="SV Write Csg.vi" Type="VI" URL="../SharedVariable/SV Write Csg.vi"/>
			<Item Name="SV Write Cxt Array.vi" Type="VI" URL="../SharedVariable/SV Write Cxt Array.vi"/>
			<Item Name="SV Write Cxt.vi" Type="VI" URL="../SharedVariable/SV Write Cxt.vi"/>
			<Item Name="SV Write Dbl Array.vi" Type="VI" URL="../SharedVariable/SV Write Dbl Array.vi"/>
			<Item Name="SV Write Dbl.vi" Type="VI" URL="../SharedVariable/SV Write Dbl.vi"/>
			<Item Name="SV Write Ext Array.vi" Type="VI" URL="../SharedVariable/SV Write Ext Array.vi"/>
			<Item Name="SV Write Ext.vi" Type="VI" URL="../SharedVariable/SV Write Ext.vi"/>
			<Item Name="SV Write I8 Array.vi" Type="VI" URL="../SharedVariable/SV Write I8 Array.vi"/>
			<Item Name="SV Write I8.vi" Type="VI" URL="../SharedVariable/SV Write I8.vi"/>
			<Item Name="SV Write I16 Array.vi" Type="VI" URL="../SharedVariable/SV Write I16 Array.vi"/>
			<Item Name="SV Write I16.vi" Type="VI" URL="../SharedVariable/SV Write I16.vi"/>
			<Item Name="SV Write I32 Array.vi" Type="VI" URL="../SharedVariable/SV Write I32 Array.vi"/>
			<Item Name="SV Write I32.vi" Type="VI" URL="../SharedVariable/SV Write I32.vi"/>
			<Item Name="SV Write I64 Array.vi" Type="VI" URL="../SharedVariable/SV Write I64 Array.vi"/>
			<Item Name="SV Write I64.vi" Type="VI" URL="../SharedVariable/SV Write I64.vi"/>
			<Item Name="SV Write Sgl Array.vi" Type="VI" URL="../SharedVariable/SV Write Sgl Array.vi"/>
			<Item Name="SV Write Sgl.vi" Type="VI" URL="../SharedVariable/SV Write Sgl.vi"/>
			<Item Name="SV Write Str Array.vi" Type="VI" URL="../SharedVariable/SV Write Str Array.vi"/>
			<Item Name="SV Write Str.vi" Type="VI" URL="../SharedVariable/SV Write Str.vi"/>
			<Item Name="SV Write U8 Array.vi" Type="VI" URL="../SharedVariable/SV Write U8 Array.vi"/>
			<Item Name="SV Write U8.vi" Type="VI" URL="../SharedVariable/SV Write U8.vi"/>
			<Item Name="SV Write U16 Array.vi" Type="VI" URL="../SharedVariable/SV Write U16 Array.vi"/>
			<Item Name="SV Write U16.vi" Type="VI" URL="../SharedVariable/SV Write U16.vi"/>
			<Item Name="SV Write U32 Array.vi" Type="VI" URL="../SharedVariable/SV Write U32 Array.vi"/>
			<Item Name="SV Write U32.vi" Type="VI" URL="../SharedVariable/SV Write U32.vi"/>
			<Item Name="SV Write U64 Array.vi" Type="VI" URL="../SharedVariable/SV Write U64 Array.vi"/>
			<Item Name="SV Write U64.vi" Type="VI" URL="../SharedVariable/SV Write U64.vi"/>
			<Item Name="SV Write Variant Array.vi" Type="VI" URL="../SharedVariable/SV Write Variant Array.vi"/>
			<Item Name="SV Write Variant.vi" Type="VI" URL="../SharedVariable/SV Write Variant.vi"/>
			<Item Name="SV Write Wfm Array.vi" Type="VI" URL="../SharedVariable/SV Write Wfm Array.vi"/>
			<Item Name="SV Write Wfm.vi" Type="VI" URL="../SharedVariable/SV Write Wfm.vi"/>
		</Item>
	</Item>
	<Item Name="CSPP_TestStand.lvclass" Type="LVClass" URL="../CSPP_TestStand/CSPP_TestStand.lvclass"/>
</Library>
