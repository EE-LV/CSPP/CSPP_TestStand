Release Notes for the CSPP_TestStand - 25-March-2019
=========================================================
This package can be used to interface CS++ with NI TestStand.

Version 1.0.0.4
===============
This is the first release based on LabVIEW 2018 and TestStand 2017 illustrating the usage.
Shared variable interface is supported and one CSPP_InstrSim callback message.
