CSPP_TestStand
==============
This package can be used to interface CS++ with NI TestStand to execute sequences using shared variables or CS++ asyncronous callback messages.

Dependencies: 
- CSPP_InstrSim, https://git.gsi.de/EE-LV/CSPP/CSPP_DN/CS_Workshop.git
- InstrSim, https://git.gsi.de/EE-LV/Drivers/InstrSim.git

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.
Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl